<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>HELPXP - Dashboard</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
<style>
	img {
    transition:transform 0.25s ease;
	}

	img:hover {
    -webkit-transform:scale(1.5); /* or some other value */
    transform:scale(1.5);
	}

	html,
body {
  height: 100%;
}

.container {
  display: table;
  width: 100%;
  height: 100%;
}

.interior {
  display: table-cell;
  vertical-align: middle;
  text-align: center;
}

.btn {
  background-color: #fff;
  padding: 1em 3em;
  border-radius: 3px;
  text-decoration: none;
}

.modal-window {
  position: fixed;
  background-color: rgba(255, 255, 255, 0.15);
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 999;
  opacity: 0;
  pointer-events: none;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  transition: all 0.3s;
}

.modal-window:target {
  opacity: 1;
  pointer-events: auto;
}

.modal-window>div {
  width: 400px;
  position: relative;
  margin: 10% auto;
  padding: 2rem;
  background: rgba(139, 130, 125, 0.54);
  color: #444;
}

.modal-window header {
  font-weight: bold;
}

.modal-close {
  color: #aaa;
  line-height: 50px;
  font-size: 80%;
  position: absolute;
  right: 0;
  text-align: center;
  top: 0;
  width: 70px;
  text-decoration: none;
}

.modal-close:hover {
  color: #000;
}

</style>

</head>
<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="{{ url('/displayusers') }}"><span>Help</span>XP</a>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">{{ Auth::user()->username }}</div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<!-- <form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form> -->
		<ul class="nav menu">
			<li><a href="{{url('/displayusers')}}"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li><a href="{{ url('/displaypatients') }}"><em class="fa fa-calendar">&nbsp;</em> Patients</a></li>
			<li class="active"><a href="{{ url('/displaysponsors') }}"><em class="fa fa-bar-chart">&nbsp;</em> Sponsors</a></li>
			<li><a class="fa fa-power-off" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">&nbsp;&nbsp;Logout
                                    </a>
                                    
                                  
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            </li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Requests Voucher</h1>
			</div>
		</div><!--/.row-->
	

	<div class="panel panel-container">
	<div class="row">
	<table class="table">
	
	<th class="fixed-table-container">Name</th>
	<th class="fixed-table-container" colspan="5">Photo Receipt</th>
	<th class="fixed-table-container"></th>
	
	<!-- <th>Illness</th>
	<th>Story</th>
	<th>Amount</th> --><br>



<p id="sponsor">
	@foreach ($bill as $sponsors)
	@if($sponsors->ifReceived != "received")
		<tr class="fixed-table-container">
			<td class="fixed-table-container">{{$sponsors->accountHolder}}</td>
			<td class="fixed-table-container"><img src="{{  url('storage/picture/'.$sponsors->receipt) }}" width="100px" height="100px" /></td>
<!-- <form action="{{url('checked')}}" method="post">
{{csrf_field()}} -->
			<!-- <td class="fixed-table-container" align="center">
				<button id="check" class="btn btn-danger">Check</button>
			</td>
			<input type="hidden" name="userid" value="{{$sponsors->userid}}">
			<input type="hidden" name="billing_id" value="{{$sponsors->billing_id}}"> -->

			
			<td class="fixed-table-container" align="center">
				<div class="container">
  				<div class="interior">
    			<a class="btn" href="#{{$sponsors->billing_id}}">Check</a>
  				</div>
				</div>

				<div id="{{$sponsors->billing_id}}" class="modal-window">
  				<div>
    		<a href="#modal-close" title="Close" class="modal-close">Close</a>
    		<div class="main-content" >

        <form class="form-basic" action="{{url('/checked')}}" method="post" enctype="multipart/form-data" style="background-color: #F9F9F9">
        {{csrf_field()}}

        <input type="hidden" name="billing_id" value="{{$sponsors->billing_id}}">

            <div class="form-title-row">
                <h2><strong>Voucher Details!</strong></h2>
            </div>
            <table border="0" cellpadding="20px">
            <tr>
            <div class="form-row">
                <label>
                    <td><strong>Date</strong></td>
                   <td> <input type="text" name="title" value="{{$sponsors->updated_at->format('F d, Y')}}"></td>
                </label>
             </tr>
            </div>

 			<tr>
                  <td><strong>Account Holder</strong></td> 
                   <td> <input type="text" name="title" value="{{$sponsors->user->fname}} {{$sponsors->user->lname}}"></td>
                
			</tr>
            
			<tr>
                <label>
                    <td><strong>Photo Receipt</strong></td>
                    <td class="fixed-table-container"><center><img id="img{{$sponsors->filename}}" src="{{  url('storage/picture/'.$sponsors->filename) }}" width="100px" height="100px" /></center></td>
                </label>
             </tr>
            </table><br><br>
            <div>
                <label>
                    <span><input type="submit" class="btn btn-primary" value="Submit">
                    <input type="reset" class="btn btn-danger" value="Reset"></span>
                </label>
            </div>

        </form>

    </div>
				</div>
			</div>
			</td>
			<!--  -->
	<!-- </form> -->

		</tr>
@endif
@endforeach

</p>
</table>
				
	
				
			</div><!--/.row-->
		</div>
		
			</div><!--/.col-->
			
		</div><!--/.row-->
	</div>	<!--/.main-->

</body>
</html>

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>
		




