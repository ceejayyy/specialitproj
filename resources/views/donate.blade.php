@extends('layouts.raise')
@section('content')


<head>


    <link rel="stylesheet" href="/css/form-basic.css">
<style>
    body {
        background-image: url('/images/bgg.png');
        top right no-repeat; 
        background-attachment:fixed;
        background-size: cover;
        margin-top: 0px;
    }
    #x{
        font-size: 40px;
        color: black;
    }
</style>

</head>
<script type="text/javascript">
     $(document).ready(function (){
      var to = "";
      var total = "";
      var div=$(this).parent();
        $.ajax({
            type: 'GET',
            url: '/helpVoucher',
            data:{
                '_token': $('p[name=_token]').val()
            },
            success:function(data){

                for(var i=0; i<data.length; i++){

                    var image =  data[i].count+'&nbsp;&nbsp;'+'<img src="'+data[i].value+'" width = 300/>'+'&nbsp;&nbsp;';
                    $('#x').html(image);
                }if(data.length = 0){
                  console.log("no vouchers");
                } 
                
                console.log(data);
            },
            error:function(){
            }
        });
    });
</script>
<script>
    function recommend(){
      var to = "";
        $.ajax({
            type: 'GET',
            url: '/recommending',
            success:function(data){
                  $.ajax({
                        type: "GET",
                        url: "/rcmPatient",
                        success:function(data){
                            if(data.length == 0){
                              $('#noresult').html("NO RESULT");
                            }
                            for(var i=0; i<data.length; i++){
                                var date = data[i].created_at;
                                var dt = new Date(date);
                                var options = { year: 'numeric', month: 'long', day: 'numeric' };
                                to+=`
                                <table>
                                    <tr>
                                      <td>`+dt.toLocaleDateString("en-US", options)+`</td>
                                      <td>`+data[i].userf+` `+data[i].userl+`</td>
                                      <td>`+data[i].patientname+`</td>
                                      <td>`+data[i].voucherValue+`</td>
                                      <td>`+data[i].fname+` `+data[i].lname+`</td>
                                    </tr>
                                </table>
                                `;
                               console.log("success");
                            }$('#pntdetails').html(to);
                                  $.ajax({
                                      type: 'GET',
                                      url: '/helpVoucher',
                                      success:function(data){
                                        console.log("done");
                                          if(data.length == 0){
                                              $('#x').html("All vouchers donated!");
                                          }
                                          for(var i=0; i<data.length; i++){
                                              var image =  data[i].count+'&nbsp;&nbsp;'+'<img src="'+data[i].value+'" width = 300/>'+'&nbsp;&nbsp;';
                                              $('#x').html(image);
                                          }
                                      }
                                  });
                        }
                  });
            }
        });
    };

    // $(document).ready(function(){
    //   setInterval(recommend,5000);
    // });
</script>
<br><br>
<center><p id="x"></p></center>
Criteria<br>
-most patient donated as sponsor<br>
-patient with longest duration story<br>
-patient with highest remaining need<br>
-patient's condition<br><br><br>

<table>
	<tr>
		<th>DATE</th>
		<th>STORY BY</th>
		<th>PATIENT'S NAME</th>
		<th>VOUCHER AMOUNT</th>
		<th>SPONSOR'S NAME</th>
	</tr>
</table>
<table id="pntdetails">
  @if($sponsored_pnt->count() == 0)
    <h1 id="noresult">NO RESULT</h1>
  @endif
	@foreach($sponsored_pnt as $pnt)

  <tr>
		<td>{{$pnt->created_at->format('F d, Y')}}</td>
		<td>{{$pnt->patient->userName->fname}} {{$pnt->patient->userName->lname}}</td>
		<td>{{$pnt->patient->patientname}}</td>
		<td>{{$pnt->sponsor->voucherValue}}</td>
		<td>{{$pnt->sponsor->user->fname}} {{$pnt->sponsor->user->lname}}</td>
	</tr>
	@endforeach
</table>
<input type="submit" id="recommend" onclick="recommend()" class="btn btn-primary" value="recommend">

@endsection
