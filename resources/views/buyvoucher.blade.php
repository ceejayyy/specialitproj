

@extends('layouts.voucher')

 
@section('content')


<head>

    <!-- <link rel="stylesheet" href="/css/form-basic.css"> -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <!-- progress form -->
    <link rel="stylesheet" href="/css/fileupload2.css">
    <script src="/js/fileupload2.js"></script>
    <!-- file upload -->
    <link rel="stylesheet" href="/css/fileupload.css">
    <script src="/js/fileupload.js"></script>

    <style>
    body {

        background-image: url('/images/bgg.png');
        top right no-repeat; 
        background-attachment:fixed;
        background-size: cover;
        margin-top: 0px;

    }
        h1 {
        display: inline-block;
        box-sizing: border-box;
        color:  #4c565e;
        font-size: 24px;
        padding: 0 10px 15px;
        border-bottom: 2px solid #6caee0;
        margin: 0;}
    </style>

</head>


<br><br>

<!-- start -->
<section class="form-box" >
            <div class="container">
                
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 form-wizard">
                    
                        <!-- Form Wizard -->
                        <form  role="form" class="form-basic" action="{{url('/voucher')}}" method="post">
            {{csrf_field()}}

                            <h1 style="color:red">BUY VOUCHERS HERE</h1>
                            <p style="font-size:12px">For new users we would like to inform you that before availing vouchers make sure that you have already transfered money to our BPI bank account provided below for fast transaction in buying vouchers. Thank You!</p>
                            
                            <!-- Form progress -->
                            <div class="form-wizard-steps form-wizard-tolal-steps-4">
                                <div class="form-wizard-progress">
                                    <div class="form-wizard-progress-line" data-now-value="12.25" data-number-of-steps="4" style="width: 12.25%;"></div>
                                </div>
                                <!-- Step 1 -->
                                <div class="form-wizard-step active" style="width: 50%">
                                    <div class="form-wizard-step-icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                    <p>Vouchers</p>
                                </div>
                                <!-- Step 1 -->
                                
                                <!-- Step 4 -->
                                <div class="form-wizard-step" style="width: 50%">
                                    <div class="form-wizard-step-icon"><i class="fa fa-money" aria-hidden="true"></i></div>
                                    <p>Payment</p>
                                </div>
                                <!-- Step 4 -->
                            </div>
                            <!-- Form progress -->
                            
                            
                <!-- Form Step 1 -->
            <fieldset>

    <h4>Voucher's Denomination: <span>Step 1 - 2</span></h4>

    <u><div id="totalVoucher" style="text-align:center;width:500px;height:100px;font-size: 20pt;"></div></u>
                            
   <div style="text-align:center">
        <label>
            <img src="/images/v_100.JPG" height="28%" width="80%"/><br>
            <p style="text-align:center"><input type=number min="0" name='qty100' id='qty100' style="width: 100px"  onclick="getTotal()" /></p>
        </label>
           
        <label>
            <img src="/images/v_500.JPG" height="28%" width="80%" /><br>
            <p style="text-align:center"><input type='number' min="0" name='qty500' id='qty500' style="width: 100px" onclick="getTotal()" /></p>
        </label>
            
        <label>
            <img src="/images/v_1000.JPG" height="28%" width="80%" /><br>
            <p style="text-align:center"><input type='number' min="0" name='qty1000' id='qty1000' style="width: 100px" onclick="getTotal()" /></p>
        </label>
           
        <label>
            <img src="/images/v_5000.JPG" height="28%" width="80%" /><br>
            <p style="text-align:center"><input type='number' min="0" name='qty5000' id='qty5000' style="width: 100px" onclick="getTotal()" /></p>
        </label>
    </div>        
                                <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-next">Next</button>
                                </div>
                            </fieldset>
                            <!-- Form Step 1 -->


                           
                            <!-- Form Step 4 -->
                            <fieldset>

                                <h4>Payment Information: <span>Step 2 - 2</span></h4>
                                <div style="clear:both;"></div>

                                <center><div class="form-group">
                                    <label>HelpXP's Bank Name: <span>*</span></label>
                                    <p style="font-weight:bolder;color:grey;font-size:25px;border:1px solid #ccc;background-color: #F5F4F4; width: 200px">BPI</p>
                                </div>

                                <div class="form-group">
                                    <label>Account Name: </label>
                                  <p style="font-weight: bolder; color: grey; font-size: 25px;border: 1px solid #ccc;background-color: #F5F4F4; width: 200px">HelpXP</p>
                                </div>

                                <div class="form-group ">
                                    <label>Card Number: <span>*</span></label>
                                    <p style="font-weight: bolder; color: grey; font-size: 25px;border: 1px solid #ccc;background-color: #F5F4F4; width: 200px">10987654321</p>
                                </div>
                                </center>

                                <div class="form-group">
                                   <center><label>Receipt</label></center>
                                    <!-- <input type="file" name="receipt"> -->
                                       <div class="file-upload" style="width:85%">
    <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Upload copy of your receipt</button>

    <div class="image-upload-wrap">
    <input class="file-upload-input" name="receipt" type='file' onchange="readURL(this);" accept="image/*" />
    <div class="drag-text">
      <h3>Drag or upload photo</h3>
    </div>
  </div>
  <div class="file-upload-content">
    <img class="file-upload-image" src="#" alt="your image" />
    <div class="image-title-wrap">
      <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
    </div>
  </div>
                                </div>
                                

                                <br/>
                                <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-previous">Previous</button>
                                    <button type="submit" class="btn btn-submit">Submit</button>
                                </div>
                            </fieldset>
                            <!-- Form Step 4 -->
                        
                        </form>
                        <!-- Form Wizard -->
                    </div>
                </div>
                    
            </div>
        </section>
<br><br><br><br><br>
<!-- end -->

 
@if(Session::has('error'))
    <script>
        alert('Cant buy with that quantity');
    </script>
@elseif(Session::has('success'))
    <script>
        alert('Voucher/s has been added to your account but still needs to be checked!');
    </script>
@endif

<script>
     
function getTotal() {
    var v1 = document.getElementById("qty100").value;
    var v2 = document.getElementById('qty500').value;
    var v3 = document.getElementById('qty1000').value;
    var v4 = document.getElementById('qty5000').value;

    var total = (v1 * 100) + (v2 * 500) + (v3 * 1000) + (v4 * 5000);
    document.getElementById("totalVoucher").innerHTML =  "Total Vouchers &#8369 "+ total;
}
</script>


<br><br>
@endsection



