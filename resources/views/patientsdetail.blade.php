 @extends('layouts.patient')

@section('content')

<!-- <div class="about" style="width: 25%; height: auto">
  <div class="desc" style="background-color: lightgreen; border: 1px solid #ccc">
     requirements:
     <br>original copy of medical abstract
     <br>medical certification
     <br>Valid ID's (voter's ID, passport, etc.)
     <br>hospital bill
     <br>official quotation of breakdown of expenses
  </div>
</div>  -->

<style>
    body {
        background-image: url('/images/bgg.png');
        top right no-repeat; 
        background-attachment:fixed;
        background-size: cover;
        margin-top: 0px;
    }
    input[type=text]{
        width: 925px;
        height: 40px;
    }
    * {
    box-sizing: border-box;
}

/* Create two equal columns that floats next to each other */
.column {
    float: left;
    width: 50%;
    padding: 10px;
    height: 300px; /* Should be removed. Only for demonstration */
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

.inputDnD {
  .form-control-file {
    position: relative;
    width: 100%;
    height: 100%;
    min-height: 6em;
    outline: none;
    visibility: hidden;
    cursor: pointer;
    background-color: #c61c23;
    box-shadow: 0 0 5px solid currentColor;
    &:before {
      content: attr(data-title);
      position: absolute;
      top: 0.5em;
      left: 0;
      width: 100%;
      min-height: 6em;
      line-height: 2em;
      padding-top: 1.5em;
      opacity: 1;
      visibility: visible;
      text-align: center;
      border: 0.25em dashed currentColor;
      transition: all 0.3s cubic-bezier(.25, .8, .25, 1);
      overflow: hidden;
    }
    &:hover {
      &:before {
        border-style: solid;
        box-shadow: inset 0px 0px 0px 0.25em currentColor;
      }
    }
  }
}

// PRESENTATIONAL CSS
body {
  background-color: #f7f7f9;
}
</style>

<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/apply-form.css">
    <script src="/js/apply-js.js"></script>
    <link rel="stylesheet" href="/css/fileupload.css">
    <script src="/js/fileupload.js"></script>
    <link rel="stylesheet" href="/css/fileupload2.css">
    <script src="/js/fileupload2.js"></script>
    <script class="jsbin" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
   <!--  <link rel="stylesheet" href="/css/form-basic.css"> -->


</head>

<br><br>

    <div class="main-content" >

    <!-- angel edited -->
    <form class="form-basic" action="{{url('/singlelist')}}" method="post" enctype="multipart/form-data" style="background-color: #F9F9F9">
        {{csrf_field()}}           

<div id="main-page">
  <div class="maincontent">
    <h1>Share your story</h1>
                      
                        <h2>STEP 1</h2>
                    <div style="text-align:left;padding-left:15%;font-size: 13pt;padding-top: 15">
                        <label>Goal Title</label>
                        <p style="font-size:12px;color:black;float:right;padding-right:15%"><span>*Title of your story</span></p>
                        <input type="text" name="title" cols="100" style="border-color:#1FB264">                        
                    </div>

                    <div style="text-align:left;padding-left:15%;font-size: 13pt;padding-top: 15">
                    <label>Your story</label>
                    <p style="font-size:12px;color:black;float:right;padding-right:15%"><span id="display_count">0</span>/250 words</p>
                    <textarea name="story" id="countWord" cols="100" rows="15" style="padding-left:50px;border-color:#1FB264"></textarea><br>
                    
                     </div>

                    <div style="text-align:left;padding-left:15%;font-size: 13pt;padding-top: 15">
                        <label>Goal Amount</label>
                        <p style="font-size:12px;color:black;float:right;padding-right:15%"><span>*Amount you want to raise</span></p>
                        <input type="text" name="goal" style="border-color:#1FB264">     
                    </div>
<br>
  <a class="mainlink" style="float:right;padding-left:5px;padding-right:2px">NEXT &rarr;</a>
<br><br><br><br><br>
</div>
</div>

<div id="second-page">
  <div class="secondcontent">
    <h1>Share your story</h1>

                        <h2>STEP 2</h2>

    <div class="file-upload">
    <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Upload profile photo of your story</button>

    <div class="image-upload-wrap">
    <input class="file-upload-input" name="profile" type='file' onchange="readURL(this);" accept="image/*" />
    <div class="drag-text">
      <h3>Drag or upload photo</h3>
    </div>
  </div>
  <div class="file-upload-content">
    <img class="file-upload-image" src="#" alt="your image" />
    <div class="image-title-wrap">
      <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
    </div>
  </div>
</div>
                
        <!--     <div style="text-align:left;padding-left:15%;font-size: 13pt;padding-top: 15">
                <label>Upload photo of your story</label>
                <p style="font-size:12px;color:black;float:right;padding-right:15%"><span>*This will serve as a profile picture of your story</span></p>
                <input type="file" name="profile">           
            </div> -->

            <div style="text-align:left;padding-left:15%;font-size: 13pt;padding-top: 15">
                <label>Beneficiary Name</label>
                <p style="font-size:12px;color:black;float:right;padding-right:15%"><span>*Complete name of the beneficiary</span></p> 
                <input type="text" name="bname" style="border-color:#1FB264">
            </div>

            <div style="text-align:left;padding-left:15%;font-size: 13pt;padding-top: 15">
                <label>Cause of Admission</label>
                 <p style="font-size:12px;color:black;float:right;padding-right:15%"><span>*Illnes of the beneficiary</span></p>
                <input type="text" name="illness" style="border-color:#1FB264">
                
            </div>

              <div style="text-align:left;padding-left:15%;font-size: 13pt;padding-top: 15">
                <label>Condition</label>
                    <select name="condition" style="border-color:#1FB264">
                        <option value="1">fair</option>
                        <option value="2">undetermined</option>
                        <option value="3">critical but stable</option>
                        <option value="4">serious</option>
                        <option value="5">critical</option>                      
                    </select>
            </div>
<br>
  <a class="secondlink" style="float:right;padding-left:5px;padding-right:2px">NEXT &rarr;</a>
<br><br><br><br><br>
</div>
</div>

<div id="next-page">
  <div class="nextcontent">

    <h1>Share your story</h1>

                        <h2>STEP 3</h2>

                        <!-- sstart -->
                        <center><div style="padding-left:25%">
    <div class="container p-y-1">
  <div class="row m-b-1">
    <div class="col-sm-6 offset-sm-3">
      <button type="button" class="btn btn-primary btn-block" onclick="document.getElementById('inputFile').click()">Medical Abstract</button>
      <div class="form-group inputDnD">
        <label class="sr-only" for="inputFile">File Upload</label>
        <input type="file" name="medicalAbstract" class="form-control-file text-primary font-weight-bold" id="inputFile" accept="image/*" onchange="readUrl(this)" data-title="Drag and drop a file">
      </div>
    </div>
  </div><br>
   <div class="row m-b-1">
    <div class="col-sm-6 offset-sm-3">
      <button type="button" class="btn btn-success btn-block" onclick="document.getElementById('inputFile1').click()">Medical Certificate</button>
      <div class="form-group inputDnD">
        <label class="sr-only" for="inputFile1">File Upload</label>
        <input type="file" name="medicalCertificate" class="form-control-file text-success font-weight-bold" id="inputFile1" accept="image/*" onchange="readUrl(this)" data-title="Drag and drop a file">
      </div>
    </div>
  </div><br>
    <div class="row m-b-1">
    <div class="col-sm-6 offset-sm-3">
      <button type="button" class="btn btn-warning btn-block" onclick="document.getElementById('inputFile2').click()">Valid ID</button>
      <div class="form-group inputDnD">
        <label class="sr-only" for="inputFile2">File Upload</label>
        <input type="file" name="validID" class="form-control-file text-warning font-weight-bold" id="inputFile2" accept="image/*" onchange="readUrl(this)" data-title="Drag and drop a file">
      </div>
    </div>
  </div><br>
    <div class="row m-b-1">
    <div class="col-sm-6 offset-sm-3">
      <button type="button" class="btn btn-danger btn-block" onclick="document.getElementById('inputFile3').click()">Hospital Bill</button>
      <div class="form-group inputDnD">
        <label class="sr-only" for="inputFile3">File Upload</label>
        <input type="file" name="hospitalBill" class="form-control-file text-danger font-weight-bold" id="inputFile3" accept="image/*" onchange="readUrl(this)" data-title="Drag and drop a file">
      </div>
    </div>
  </div><br>
    <div class="row m-b-1">
    <div class="col-sm-6 offset-sm-3">
      <button type="button" class="btn btn-block btn-block" onclick="document.getElementById('inputFile4').click()">Breakdown Of Expenses</button>
      <div class="form-group inputDnD">
        <label class="sr-only" for="inputFile4">File Upload</label>
        <input type="file" name="breakdownExpenses" class="form-control-file text-danger font-weight-bold" id="inputFile4" accept="image/*" onchange="readUrl(this)" data-title="Drag and drop a file">
      </div>
    </div>
  </div><br>
</div>
</div></center>
                        <!-- end -->

            
           <!--  <div style="text-align: center;font-size: 13pt;padding-top: 15">
                <label>
                    <span>Medical Abstract</span>
                    <span style="padding-left: 0;"><input type="file" name="medicalAbstract"></span>
                </label>
            </div>
            <div style="text-align: center;font-size: 13pt;padding-top: 15">
                <label>
                    <span>Medical Certification</span>
                    <span style="padding-left: 0;"><input type="file" name="medicalCertificate"></span>
                </label>
            </div>
            <div style="text-align: center;font-size: 13pt;padding-top: 15">
                <label>
                    <span>Valid ID</span>
                    <span style="padding-left: 0;"><input type="file" name="validID"></span>
                </label>
            </div>
            <div style="text-align: center;font-size: 13pt;padding-top: 15">
                <label>
                    <span>Hospital Bill</span>
                    <span style="padding-left: 0;"><input type="file" name="hospitalBill"></span>
                </label>
            </div>
            <div style="text-align: center;font-size: 13pt;padding-top: 15">
                <label>
                    <span>Breakdown of Expenses</span>
                    <span style="padding-left: 0;"><input type="file" name="breakdownExpenses"></span>
                </label>
            </div> -->
             <div>
                <center><label>
                    <span style="font-color: white"><input type="submit" class="btn btn-primary" value="Submit">
                    <input style="color: white" type="reset" class="btn btn-danger" value="Reset"></span>
                </label></center>
            </div>
<br>
    <a class="nextlink" style="float:right;"> &larr; GO BACK</a>
<br><br><br><br><br>
</div>
</div>

</div>
</form>
    <!-- angel end -->

</body>

</html>

<script>
    $(document).ready(function() {
  $("#countWord").on('keyup', function() {
    var words = this.value.match(/\S+/g).length;

    if (words > 250) {
      // Split the string on first 200 words and rejoin on spaces
      var trimmed = $(this).val().split(/\s+/, 250).join(" ");
      // Add a space at the end to make sure more typing creates new words
      $(this).val(trimmed + " ");
    }
    else {
      $('#display_count').text(words);
      $('#word_left').text(250-words);
    }
  });
}); 
</script>

<script>
    function readUrl(input) {
  
  if (input.files && input.files[0]) {
    let reader = new FileReader();
    reader.onload = (e) => {
      let imgData = e.target.result;
      let imgName = input.files[0].name;
      input.setAttribute("data-title", imgName);
      console.log(e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }

}
</script>

@endsection 




