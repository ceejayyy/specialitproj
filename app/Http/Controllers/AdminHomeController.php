<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Requests;

use Auth;
use App\User;
use App\Stories;
use App\Picture;
use App\Patient;
use App\Sponsor;
use App\Donation;
use App\Billing;
use Carbon\Carbon;
use DB;


class AdminHomeController extends Controller
{
    public function adminLogin(Request $req) {
        //return $req;
        if (Auth::attempt(['username' => $req->username, 'password' => $req->password, 'role' => 'admin']))
        {

            return view('displayUsers');
        }
        else
            return view('auth.admin-login');
    }

    public function viewUsers()
    {

        if(Auth::user()->role == "admin"){
            $recommendation = Patient::where('status', 'approved')->orWhere('status', 'partial')->whereRaw('goal != TotalRedeem')->get();

            $data = Patient::where('status', 'approved')->orWhere('status', 'partial')->whereRaw('goal != TotalRedeem')->get();

            $success = Patient::whereRaw('goal = TotalRedeem')->get();
    
            return view('displayusers')->with(['data'=>$data, 'success'=>$success, 'recommendation'=>$recommendation]);
    }

        else
            "ERROR";

    }

    // public function deleteUsers(Request $request)
    // {
    //    $user = User::findOrFail($request->id);
    //    $user->delete();
    //    return redirect(url('/displayusers'));
    // }

    public function viewPatients()
    {
        if(Auth::user()->role == "admin"){
            $users = Patient::select('userid')->distinct('userid')->get();
            return view('displaypatients', compact('users'));
    }
        else
            return "ERROR!";
    }

    public function viewSponsors()
    {
        if(Auth::user()->role == "admin"){
            $users = Sponsor::select('userid')->distinct('userid')->get();
            return view('displaysponsors', compact('users'));
            }
        else
            return "ERROR!";
    }
    

    public function patientSponsor($userid) {

       //   $id = Donation::where('patientid', '=', $userid)->get();
       // return view('displayPatientSponsor')->with([ 'patientid' => $id]);

        if(Auth::user()->role == "admin"){
        $patient = Patient::where('userid', $userid)->get();
        $voucher = Donation::get();
        $patientCollect = new Collection();
        foreach($patient as $pts) {
            foreach ($voucher as $vcr) {
                if($pts['patientid'] == $vcr['patientid']) {
                    $patientCollect->push($vcr);
                 
                }
            }
            
        }
      
       return view('displayPatientSponsor')->with([ 'patientCollect' => $patientCollect]);
    }
    else
        return "ERROR!";

    }

    public function sponsorSponsored($userid) {
        if(Auth::user()->role == "admin"){
        $sponsor = Sponsor::where('userid', $userid)->get();
        $voucher = Donation::get();
        $sponsorCollect = new Collection();
        foreach($sponsor as $spr){
            foreach($voucher as $dnr){
                if($dnr['sponsor_serial'] == $spr['sponsor_serial']){
                $sponsorCollect->push($spr);
                }
            }
        }

        return view('displaySponsorSponsored')->with(['sponsorCollect'=>$sponsorCollect]);
    }

    else return "ERROR!";
    }

    public function patienthistory($userid) {
        if(Auth::user()->role == "admin"){
        $user = Auth::id();
        $patients = Patient::where('userid', "=" ,$userid)->get();
        return view('patienthistory')->with(['user'=>$patients]);
    }
        else 
            return "ERROR";
    }

    public function sponsorhistory($userid) {
        if(Auth::user()->role == "admin"){
        $user = Auth::id();
        $sponsors = Sponsor::where('userid', "=" ,$userid)->get();
        return view('sponsorhistory')->with(['user'=>$sponsors]);
         }
        else 
            return "ERROR";
    }

    public function approveStories() {
        if(Auth::user()->role == "admin"){
            $patient = Patient::where('status', 'pending')->get();
            $approve = new Collection();
            foreach($patient as $p){
                $story = $p->stories[0];
                $approve->push($story);
            }
            return view('approveStories')->with(['approve'=>$approve]);
        }
        else 
            return "ERROR";

    }

    public function approved(Request $request) {
        if(Auth::user()->role == "admin"){
            $pnt = Patient::findOrFail($request->patientid);
            $pnt->status = "approved";
            $pnt->save();

            $patient = Patient::where('status', 'pending')->get();
            $approve = new Collection();
            foreach($patient as $p){
                $story = $p->stories[0];
                $approve->push($story);
            }
            return view('approveStories')->with(['approve'=>$approve]);
        }

        else 
            return "ERROR";

    }

    

    public function filterSponsorDate(Request $request){ 
        if(Auth::user()->role == "admin"){
        $from = $request->from;
        $to = $request->to;
        $users = sponsor::select('userid')->distinct('userid')->whereBetween(DB::raw('DATE(created_at)'), array($from, $to))->get();
         return view('displaySponsors')->with(['users'=>$users]);
         }
        else 
            return "ERROR";
    }

    public function requestRedeem()
    {
        if(Auth::user()->role == "admin") {
         $patient = Patient::where('status', 'partial')->orWhere('status', 'full')->get();
            $redeem = new Collection();
            foreach($patient as $p){
                $pnt = $p->stories[0];
                $redeem->push($pnt);
            }

        $user = Auth::id();    
$released = Patient::where('userid', $user)->where('status', 'partial')->orWhere('status', 'full')->get();

$expirydate = date('Y-m-d', strtotime('+1 years'));
            return view('reqRedeems')->with(['redeem'=>$redeem, 'released'=>$released, 'expirydate'=>$expirydate]);
        }
        else
            "ERROR";
    }

    public function reqConfirm(Request $request){
        $details = Patient::findorfail($request->patientid);
        $total = $details->redeemed;
        // $details = Patient::where('userid', $user)->where('status', 'approved')->get();
        $expirydate = date('d M Y', strtotime('+1 years'));
        return view('confirmreq')->with(['details'=>$details, 'total'=>$total, 'expirydate'=>$expirydate]);
    }


    public function requestApproveRedeem(Request $request)
    {
        if(Auth::user()->role == "admin"){
        $patient = Patient::where('status', 'partial')->orWhere('status', 'full')->get();

            $pnt = Patient::findOrFail($request->patientid);
            $pnt->redeemStatus = "For release";
            $pnt->save();
            
            $redeem = new Collection();
            foreach($patient as $p){
                $pnt = $p->stories[0];
                $redeem->push($pnt);
            }

            return view('reqApproveRedeems')->with(['redeem'=>$redeem]);
            }

        
       
        else
            return "ERROR!";

    }

    public function filterPatientDate(Request $request){ 
        if(Auth::user()->role == "admin"){
        $from = $request->from;
        $to = $request->to;
        $users = patient::select('userid')->distinct('userid')->whereBetween(DB::raw('DATE(created_at)'), array($from, $to))->get();
         return view('displayPatients')->with(['users'=>$users]);
         }
        else 
            return "ERROR";
    }

        // angel
    public function checkPayment() {
        if(Auth::user()->role == "admin"){
            $bill = Billing::where('ifReceived','=','pending')->get();
            $check = new Collection();
            foreach($bill as $bills){
                $candonate = $bills->sponsors[0];
                $check->push($candonate);
            }
            return view('check')->with(['bill'=>$bill]);
        }
        else 
            return "ERROR";

    }

    public function checked(Request $request) {
        if(Auth::user()->role == "admin"){
            $bill = Billing::find($request->billing_id);
            $bill->ifReceived = "received";
            $bill->save();

            $sponsor = Sponsor::where('billing_id', $request->billing_id)->get();
            foreach ($sponsor as $sponsors) {
              $sponsors->status = null;
              $sponsors->save();
            }           

            $sponsor = Sponsor::where('status', null)->get();
            $approve = new Collection();
            foreach($sponsor as $sponsors){
                $candonate = $sponsors->sponsors[0];
                $approve->push($candonate);
            }
            return view('checkedPayment')->with(['sponsor'=>$sponsor]);
        }

        else 
            return "ERROR";
    }

    public function search(Request $request) {

         $patient = Patient::where('status', 'partial')->orWhere('status', 'full')->get();
            $redeem = new Collection();
            foreach($patient as $p){
                $pnt = $p->stories[0];
                $redeem->push($pnt);

        $search = $request->search;
        $patients = Patient::where('patientname', 'like', '%'.$search.'%')->get();

            return view('displaySearch')->with(['patients'=>$patients, 'redeem'=>$redeem]);

        }
    }
    public function displayStories() {
        if(Auth::user()->role == "admin"){

        $patient = Patient::get();
        $stories = new Collection();
            foreach($patient as $p){
                $pnt = $p->stories[0];
                $stories->push($pnt);
            }

        return view('displayStories')->with(['stories'=>$stories]);
        
        }

        else
            return "ERROR!";

    }

// angel new
    public function searchPatient(Request $request){
        $search = $request->search;
        $patient = Patient::where('illness','LIKE', '%' .$search. '%')->get();
        if(count($patient)>0){
          return view('search')->with(['patient' => $patient]);  
      }else
        return Redirect::back()->with('message', "No Results Found! Try another...");      
    }

    public function newStories(){
        $new = Patient::where('status','=','approved')->whereDate('created_at',Carbon::today())->get();
        if(count($new)>0){
            return view('newStories')->with(['new' => $new]);
        } else
        return Redirect::back()->with('noNew', "There are no new stories as of the moment! Try another...");    
    }
 //end

public function donate(){

$yesterday = date('Y-m-d', time() - 60 * 60 * 24);
//list of recommended patients
$sponsored = Sponsor::select('sponsor_serial')->where('status', 'dd-hx')->get();
$sponsored_pnt = Donation::with('sponsor', 'patient')->whereIn('sponsor_serial', $sponsored)
->whereDate('created_at','=', $yesterday)
->get();
return view('donate')->with(['sponsored_pnt'=>$sponsored_pnt]);       
}



public function helpVoucher(){
    $overall = 0;
    $donation = Donation::select('sponsor_serial')->where('sponsor_serial', '!=', null)->get();
    $tohelpxp = Sponsor::select('voucherValue')->distinct()->where('status', 'd-hx')->whereNotIn('sponsor_serial', $donation)->orderBy('voucherValue', 'desc')->get();
    
    $d = new Collection();
     foreach ($tohelpxp as $key) {
        $cnt = Sponsor::where('voucherValue', $key->voucherValue)->where('status', 'd-hx')->whereNotIn('sponsor_serial', $donation)->get()->count();
        $total = $key->voucherValue * $cnt;
        $overall = $total+=$overall;

        if($key->voucherValue == 100){
          $value['value'] = '/images/v_100.jpg';
        }else if($key->voucherValue == 500){
          $value['value'] = '/images/v_500.jpg';
        }else if($key->voucherValue == 1000){
          $value['value'] = '/images/v_1000.jpg';
        }else if($key->voucherValue == 5000){
          $value['value'] = '/images/v_5000.jpg';
        }

        $value['count'] = $cnt;
        $value['total'] = (string)$overall;
        $d->push($value);    
      }

return $d;
}

public function recommending(){
$donation = Donation::select('sponsor_serial')->where('sponsor_serial', '!=', null)->get();
$tohelpxp = Sponsor::where('status', 'd-hx')->whereNotIn('sponsor_serial', $donation)->orderBy('voucherValue', 'desc')->get();


$sum = $tohelpxp->sum('voucherValue');
//if tanan patient kay nahatagan na
if(Patient::where('flag', '1')->count() == Patient::count()){
    $p_all = Patient::all();
    foreach($p_all as $all){
        $save = Patient::findorfail($all->patientid);
        $save->flag = 0;
        $save->save();
    }
}
//CRITERIA
//*most sponsor donated
$mostDonated = Sponsor::select('userid')->distinct()->where('status', 'donated')->get();

$most_donated = new Collection();
foreach($mostDonated as $md){
    $spnr = Sponsor::where('userid', $md->userid)->where('status', 'd-hx')->orWhere('status', 'donated')->orWhere('status', 'dd-hx')->count();
    $most['count'] = $spnr;
    $most['userid'] = $md->userid;
    $most_donated->push($most);
}
$sort_most_donated = $most_donated->sortByDesc('count');
//*find patients under this userid's
$patient_useridd = new Collection();
foreach($sort_most_donated as $most_d){
    $pnts = Patient::where('userid', $most_d['userid'])->where('flag', '0')->get();
    foreach($pnts as $p){
        $patient_useridd->push($p); 
    }
}
$patient_userid = $patient_useridd->take(15);
$patient_withdonation = new Collection();
foreach($patient_userid as $pd){
    $patient_withdonation->push($pd->patientid);
}


if($patient_userid->count() <= 15){
    $take = 25 - $patient_userid->count();
$patient_nodonation = Patient::whereNotIn('patientid', $patient_withdonation)->get();
$p_createdat = $patient_nodonation->sortBy('created_at');
$p_highest = new Collection();
foreach($p_createdat as $created_at){
    $lacking = $created_at->goal - $created_at->TotalRedeem;
    $created_at['lacking'] = $lacking;
    $p_highest->push($created_at);
}
$p_highestneed = $p_highest->sortByDesc('lacking');
$p_condition = $p_highestneed->sortBy('condition');
$p_nodonation = $p_condition->take($take);
    foreach($patient_nodonation as $pnd){
        $patient_userid->push($pnd);
    }
}


//*order by longest duration
$longest_duration = $patient_userid->sortby('created_at');
$take_lduration = $longest_duration->take(20);
//highest need
$highest= new Collection();
foreach($take_lduration as $p){
    $lacking = $p->goal - $p->TotalRedeem;
    $p['lacking'] = $lacking;
    $highest->push($p);
}
$highest_need = $highest->sortByDesc('lacking');
$take_highestneed = $highest_need->take(15);
//*order by condition
$condition = $take_highestneed->sortBy('condition');
$take_condition = $condition->take(10);

$list = new Collection();
foreach($take_condition as $pnt){
    $list->push($pnt);
}
//RECOMMENDATION
foreach($tohelpxp as $v){ 
    $cnt = 0;
    foreach($list as $p){
        if($p->flag == 1){
            $cnt++;
        }
    }
    if($cnt == $list->count()){
        foreach($list as $p){
            echo $p->flag."<br>";
            $p->flag = 0;
            echo $p->flag."<br>";
        }
    }
    foreach($list as $p){
           
        if($p->lacking - $v->voucherValue >= 0 && $v->status == "d-hx" && $p->flag == 0){
            echo $p->patientid."  ";
            echo $v->voucherValue."  ";
            echo $p->TotalRedeem."  ";
            $p->TotalRedeem = $p->TotalRedeem + $v->voucherValue;
            $p->lacking = $p->lacking - $v->voucherValue;
            $v->status = 'dd-hx';
            $p->flag = 1;
            echo $p->TotalRedeem."  ";
            echo $v->status."  ";
            echo $p->flag."  ";
            echo $p->lacking."<br>";
            
            $sponsor = Sponsor::findOrFail($v->sponsor_serial);
            $sponsor->status = 'dd-hx';
            $sponsor->save();

            $patient = Patient::findOrFail($p->patientid);
            $newTotal = $patient->TotalRedeem + $v->voucherValue;
            $patient->TotalRedeem = $newTotal;
            $patient->flag = 1;
            $patient->save();

            $donation = new Donation();
            $donation->patientid = $p->patientid;
            $donation->sponsor_serial = $v->sponsor_serial;
            $comName = $v->user->fname." ".$v->user->lname;
            $donation->sponsorName = $comName;
            $donation->save();

            break;
        }
    }
}
}

public function rcmPatient(){
 $yesterday = date('Y-m-d', time() - 60 * 60 * 24);
 $today = date('Y-m-d');
    $sponsored_pnt = DB::table('donations')
    ->join('sponsors', 'donations.sponsor_serial', 'sponsors.sponsor_serial')
    ->join('patients', 'donations.patientid', 'patients.patientid')
    ->join('users as s', 's.id', 'sponsors.userid')
    ->join('users as p', 'p.id', 'patients.userid')
    ->where('sponsors.status', '=', 'dd-hx')
    ->whereBetween(DB::raw('DATE(donations.created_at)'), array($today, $today))
    ->select('donations.created_at', 's.fname', 's.lname', 'patients.patientname', 'sponsors.voucherValue', 'p.fname as userf', 'p.lname as userl')
    ->get();
    return $sponsored_pnt;

}
}
